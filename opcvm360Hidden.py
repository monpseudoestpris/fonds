#!/usr/bin/env python3

# Pre-requesites:
#   sudo pip3 install selenium
#   sudo apt-get install phantomjs

import argparse
import datetime
import inspect
import json
import logging as l
import re
import os
import selenium
from selenium import webdriver
from selenium.common.exceptions import ElementNotVisibleException
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from selenium.webdriver.remote.command import Command
from selenium.webdriver.support.select import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import random

import sys
import time
import psutil
SLEEPMAX=30

import subprocess as s


NBRPAGES=1000
LONGUEURMIN=600

#LFONDS=["EUR","USD","JPY","CHF","GBP"]
#LFONDS=["EUR","USD"]


class Main():
    log_level_default = l.INFO

    def __init__(self,market,poche):
        self.market=market
        self.poche=poche
        self.p="all"
        if self.poche=="PEA":
            self.p="2"
        if self.poche=="AV":
            self.p="3"   
        if self.poche=="ALL":
            self.p="all"
        self.__TAG = __file__[__file__.rfind(os.sep)+1:]
        tag =  self.__TAG + "::" + inspect.stack()[0][3] + ":: "
        time_current = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S_%f")
        l.debug(tag + "script started at: " + time_current)
        my_dir = os.environ["HOME"] + os.sep + '.' + self.__name()
        my_conf_filename ="opcvm360.cfg"
        session_id = None
        print(my_dir + os.sep + my_conf_filename)
        if os.path.exists(my_dir + os.sep + my_conf_filename):
            session_id = open(my_dir + os.sep + 
                              my_conf_filename, 'r').readline()
            print("Existing browser session is available: sessionid ==== "
            + session_id)
        else:
            print("No existing configuration file.")

        chrome_options = Options()
        headless=True
        if headless:
            chrome_options.add_argument("--headless")
            chrome_options.add_argument("--window-size=1920x1080")
        driver = webdriver.Chrome(os.environ['HOME'] + os.sep +
                                  'bin/chromedriver/chromedriver',
                                  chrome_options=chrome_options)
        #chrome_options.add_argument("--headless")

        self.__driver = driver
                    #, session_id=session_id)

        command_executor_url = self.__driver.command_executor._url
        print("browser url =" + command_executor_url)

        open(my_dir + os.sep + my_conf_filename, 'w').write(self.__driver.session_id)

        for m in [self.market]:

            res = ""
            resratios=""
            resglissantes=""
            links=""
            longueur=9999999
            i=0
            while(longueur>LONGUEURMIN):
                longueur=0
                print("==============================================================================================================================================================================================",i,"=====================================================================================")
                time.sleep(random.randint(1,SLEEPMAX))
                what="perf"
                r = self.get_fund(i+1,m,what)
                res+=r
                longueur +=len(r)
                # for lii in li:
                #     links+=str(lii[0].replace("Fiche d'identité ",""))+";"+str(lii[1])+"\n"
                time.sleep(random.randint(1,SLEEPMAX))
                what="ratios"
                r = self.get_fund(i+1,m,what)
                resratios+=r
                longueur +=len(r)
                time.sleep(random.randint(1,SLEEPMAX))
                what="glissantes"
                r = self.get_fund(i+1,m,what)
                resglissantes+=r
                longueur +=len(r)
                print("longueur=",longueur)
                i+=1


            fo = open('perfos'+m+'.txt', 'w')
            #fl= open('linksOPCVM'+m+'.txt', 'w')
            fratios=open('ratios'+m+'.txt', 'w')
            fglissantes=open('perfglissantes'+m+'.txt', 'w')
            #fl.write(links)
            fo.write(res)
            fratios.write(resratios)
            fglissantes.write(resglissantes)



#TBD: nouveau lien https://funds360.euronext.com/opcvm/palmares/categoryOpcvm/all/fundType/all/universe/all/sgp/all/currency/EUR/isr/all/licontract/all/customized/0/by/perf/page/1/sortfield/varPYTD/order/DESC
    def get_fund(self, i,money,what):
        attempts = 1
        #URL="https://www.opcvm360.com/opcvm/palmares-de-fonds-opcvm360/categoryOpcvm/all/fundType/all/universe/all/sgp/all/currency/"+money+"/licontract/all/by/perf/page/"+str(i)+"/sortfield/varPYTD/order/DESC"
        URL="https://funds360.euronext.com/opcvm/palmares/categoryOpcvm/all/fundType/all/universe/"+self.p+"/sgp/all/currency/"+money+"/licontract/all/by/perf/page/"+str(i)+"/sortfield/varPYTD/order/DESC"
        URLratios="https://funds360.euronext.com/opcvm/palmares/categoryOpcvm/all/fundType/all/universe/"+self.p+"/sgp/all/currency/"+money+"/licontract/all/by/ratio/page/"+str(i)+"/sortfield/ter/order/DESC"
        URLperfg="https://funds360.euronext.com/opcvm/palmares/categoryOpcvm/all/fundType/all/universe/"+self.p+"/sgp/all/currency/"+money+"/licontract/all/by/perf-short/page/"+str(i)+"/sortfield/varP7D/order/DESC"
        ID="sortable-perf"
        IDratios="sortable-ratios"
        IDperfg="sortable-perf-short"

        if what=="perf":
            url=URL
            id=ID
        if what=="ratios":
            url=URLratios
            id=IDratios
        if what=="glissantes":
            url=URLperfg
            id=IDperfg
        print(url)
        
        #URLratios="https://funds360.euronext.com/opcvm/palmares/categoryOpcvm/all/fundType/all/universe/all/sgp/all/currency/EUR/isr/all/licontract/all/customized/0/by/ratio/page/1/sortfield/ter/order/DESC"
        #URLperfosglissantes="https://funds360.euronext.com/opcvm/palmares/categoryOpcvm/all/fundType/all/universe/all/sgp/all/currency/EUR/isr/all/licontract/all/customized/0/by/perf/page/1/sortfield/varP7D/order/DESC"


        print('requesting url:' + url)
        res = ""
        

        
        
        
        while attempts < 100:
            print("attempt",attempts)
            self.__driver.implicitly_wait(random.randint(2+attempts,SLEEPMAX*attempts)) # seconds
            try:
                self.__driver.get(url)
                self.__driver.implicitly_wait(random.randint(2+attempts,SLEEPMAX*attempts)) # seconds
                web_element = self.__driver.find_element_by_id(id)#pour les ratios sortable-ratios
                self.__driver.implicitly_wait(attempts)
                res = web_element.text
                break
            except:
                attempts += 1
        

        print("result length",len(res))
        print(res)

        #self.__driver.close()
        #try:
            #self.__driver.quit()
            #print("Quited driver")
        #except:
            #pass
        # linksout=[]
        # links=web_element.find_elements_by_class_name('substr')

        # for l in links:
        #     ll=l.get_attribute('href')
        #     lll=l.get_attribute('title')
        #     linksout.append([lll,ll])

        

        return (res + os.linesep)

    def __name(self):
        pos = __file__.rfind(os.sep)+1
        name_with_py_ext = __file__[pos:]
        name = re.sub('\.py$', '', name_with_py_ext)
        assert name !=  name_with_py_ext, "can't find python .py extension"

        return name



def log_path():
    script_filename = os.path.abspath(__file__)
    pos = script_filename.rfind(os.sep) + 1
    script_filename = script_filename[pos:]
    base_path = re.sub('\.py$', '', script_filename)
    assert base_path != script_filename, "can't find python .py extension"
    base_path = '.' + base_path
    base_path = os.environ['HOME'] + os.sep + base_path
    os.makedirs(base_path, exist_ok=True)
    return base_path + os.sep + 'log.txt'

def main(args):
    
    Main(args.market,args.poche)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="script description")
    parser.add_argument("market", help="market to parse (e.g., EUR, USD)")
    parser.add_argument("poche", help="poche to invest (e.g., ALL, PEA, AV)")
    args = parser.parse_args()
    main(args)

