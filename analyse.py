#!/usr/bin/env python3

import argparse
import os
import re
import math
from decimal import Decimal
import sys
import xlsxwriter
import random
import datetime

DOGETCATS=True
NB=3000
NBPERFO=10

DFT_SCORE1MOIS=0.01
DFT_SCORE3MOIS=0.01
DFT_SCORE6MOIS=0.01

DFT_SCORE1AN=0.3
DFT_SCORE3ANS=0.3
DFT_SCORE5ANS=0.3
DFT_SCORE10ANS=0.1-DFT_SCORE1MOIS-DFT_SCORE3MOIS-DFT_SCORE6MOIS

DFT_SHARP1AN=0.25
DFT_SHARP3ANS=0.25
DFT_SHARP5ANS=0.25
DFT_SHARP10ANS=0.25

DFT_SHARPMIN=0.5
DFT_SHARPMAX=2.0
INFINITE=999999
MAXVALUE=900
ZERO=0.0
DEBUG=False

DELTA=1

listreplace=["Actions","italisations","-","logie","ernatif","ectionnelle","mmation","munication","/","reprises","-","_","érales","reprises","1","2","3","4","5","6","7","8","9","0"]

listecarreplace=["'",'`',"!","?",",",";",":","''"]
changes=[("é",'e'),("è",'e'),("à","a"),(" ","")
    ]


def get_cat_connues():
    hash_cat_connues={}
    lines=[x.split(':') for x in open("sortie_parse.txt").readlines()]
    for l in lines:
        try:
            hash_cat_connues[l[0].strip()]=l[1].strip()
        except:
            pass
    return(hash_cat_connues)

def shortenname(sheetname):
    for r in listreplace:
        sheetname=sheetname.replace(r,"")
    for r in changes:
        sheetname=sheetname.replace(r[0],r[1])
    sheetname=sheetname.lower()
    replace_nocat="No category"
    if len(sheetname)<4:
        sheetname=replace_nocat
    return(sheetname)

def getperfo(c,year):
    c=(float)(c)
    if c>MAXVALUE:
        c=0    
    try:
        valeur=1+c/100.0
    except:
        valeur=1.0
    
    out=(math.pow(valeur,1/year)-1)*100
    if out>MAXVALUE/2:
        out=0
    return(out)
def strFund(l):
    out=l[0]+" "+l[1]+" "+(str)(l[-1])
    return(out)

class Fonds:
    """Class to describe a fund"""

    def __init__(self,nom,vl,p1janv,p7j,p1mois,p3mois,p6mois,p1an,p3ans,p5ans,p10ans):
        self.nom=nom
        self.valorisation=vl
        self.perfo_1janv=p1janv
        self.perfo_7j=p7j
        self.perfo_1mois=p1mois
        self.perfo_3mois=p3mois
        self.perfo_6mois=p6mois
        self.perfo_1an=p1an
        self.perfo_3ans=p3ans
        self.perfo_5ans=p5ans
        self.perfo_10ans=p10ans
        self.categorie="No category"
    def display(self):
        try:
            l=[self.nom,self.categorie,self.valorisation,self.perfo_1janv,self.perfo_7j,self.perfo_1mois,self.perfo_3mois,self.perfo_6mois,self.perfo_1an,self.perfo_3ans,self.perfo_5ans,self.perfo_10ans, self.vol1an,self.vol3ans,self.vol5ans,self.sharp1an,self.sharp3ans,self.sharp5ans]
            ls = [(str)(x) for x in l]
            s=";".join(ls)
        except:
            s=""
        return(s)
    def add_categorie(self,c):
        c=c.replace("\n","")
        self.categorie=c
    def add_volasharp(self,v1,v2,v3,s1,s2,s3):
        self.vol1an=v1
        self.vol3ans=v2
        self.vol5ans=v3

        self.sharp1an=s1
        self.sharp3ans=s2
        self.sharp5ans=s3

def get_field(list,number,defaut):
    try:
        out=(float)(list[number])
    except:
        out=defaut
    return(out)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="convert raw funds file to CSV")
    parser.add_argument("market", help="market to parse (e.g., EUR, USD)")
    parser.add_argument("params", help="params (score1mois/score3mois/score6mois/score1an/score3ans/score5ans/score10ans/sharp1an/sharp3ans/sharp5ans/sharpmin/sharpmax)")
    args = parser.parse_args()
    money=args.market
    params=args.params.split("/")
    if len(params)<12:
        SCORE1MOIS=DFT_SCORE1MOIS
        SCORE3MOIS=DFT_SCORE3MOIS
        SCORE6MOIS=DFT_SCORE6MOIS
        SCORE1AN=DFT_SCORE1AN
        SCORE3ANS=DFT_SCORE3ANS
        SCORE5ANS=DFT_SCORE5ANS
        SCORE10ANS=DFT_SCORE10ANS
        SHARP1AN=DFT_SHARP1AN
        SHARP3ANS=DFT_SHARP3ANS
        SHARP5ANS=DFT_SHARP5ANS
        SHARPMIN=DFT_SHARPMIN
        SHARPMAX=DFT_SHARPMAX
    else:
        try:
            SCORE1MOIS=(float)(params[0])
            SCORE3MOIS=(float)(params[1])
            SCORE6MOIS=(float)(params[2])
            SCORE1AN=(float)(params[3])
            SCORE3ANS=(float)(params[4])
            SCORE5ANS=(float)(params[5])
            SCORE10ANS=(float)(params[6])
            SHARP1AN=(float)(params[7])
            SHARP3ANS=(float)(params[8])
            SHARP5ANS=(float)(params[9])
            SHARPMIN=(float)(params[10])
            SHARPMAX=(float)(params[11])
        except:
            CORE1MOIS=DFT_SCORE1MOIS
            SCORE3MOIS=DFT_SCORE3MOIS
            SCORE6MOIS=DFT_SCORE6MOIS
            SCORE1AN=DFT_SCORE1AN
            SCORE3ANS=DFT_SCORE3ANS
            SCORE5ANS=DFT_SCORE5ANS
            SCORE10ANS=DFT_SCORE10ANS
            SHARP1AN=DFT_SHARP1AN
            SHARP3ANS=DFT_SHARP3ANS
            SHARP5ANS=DFT_SHARP5ANS
            SHARPMIN=DFT_SHARPMIN
            SHARPMAX=DFT_SHARPMAX

    print("params:\n======\n",SCORE1MOIS,"\n",SCORE3MOIS,"\n",SCORE6MOIS,"\n",SCORE1AN,"\n",SCORE3ANS,"\n",SCORE5ANS,"\n",SCORE10ANS,"\n",SHARP1AN,"\n",SHARP3ANS,"\n",SHARP5ANS,"\n",SHARPMIN,"\n",SHARPMAX,"\n")

    datetoday=(str)(datetime.datetime.today()).split(" ")[0]
    workbook = xlsxwriter.Workbook(money+datetoday+'.xlsx')
    lines_money=[x.split(';') for x in open('sortie'+args.market+'.txt','r').readlines()]
    listeFonds=[]
    listFondsByCat={}
    listPerfoCat=[]
    for l_parsed in lines_money:
        
        try:
            nomFonds=l_parsed[0]
            categorie=l_parsed[1]
            categorie = shortenname(categorie)
            
            perfo1janv=(float)(l_parsed[3])
            perfo7j=(float)(l_parsed[4])
            perfo1mois=(float)(l_parsed[5])
            if perfo1mois>MAXVALUE:
                perfo1mois=0
            perfo3mois=(float)(l_parsed[6])
            if perfo3mois>MAXVALUE:
                perfo3mois=0
            perfo6mois=(float)(l_parsed[7])
            if perfo6mois>MAXVALUE:
                perfo6mois=0
            
            perfo1an=(float)(l_parsed[8])
            if perfo1an>MAXVALUE:
                perfo1an=0
            perfo3ans=(float)(l_parsed[9])
            if perfo3ans>MAXVALUE:
                perfo3ans=0
            perfo5ans=(float)(l_parsed[10])
            if perfo5ans>MAXVALUE:
                perfo5ans=0
            perfo10ans=(float)(l_parsed[11])
            if perfo10ans>MAXVALUE:
                perfo10ans=0
            vol1an=(float)(l_parsed[12])
            vol3ans=(float)(l_parsed[13])
            vol5ans=(float)(l_parsed[14])
            sharp1an=max((float)(l_parsed[15]),-9999999999)
            sharp3ans=max((float)(l_parsed[16]),-9999999999)
            sharp5ans=max((float)(l_parsed[17]),-9999999999)
         
            sharpfactor=min(max((sharp1an*SHARP1AN+sharp3ans*SHARP3ANS+sharp5ans*SHARP5ANS),SHARPMIN),SHARPMAX)
            metrique=round(Decimal((SCORE1AN*perfo1an+SCORE3ANS*perfo3ans+SCORE5ANS*perfo5ans+SCORE10ANS*perfo10ans+SCORE1MOIS*perfo1mois+SCORE3MOIS*perfo3mois+SCORE6MOIS*perfo6mois)*sharpfactor),2)
            listeFonds.append([nomFonds,categorie,perfo1an,perfo3ans,perfo5ans,perfo10ans,vol1an,vol3ans,vol5ans,sharp1an,sharp3ans,sharp5ans,perfo1mois,perfo3mois,perfo6mois,metrique])
            if not(categorie in listFondsByCat):
                listFondsByCat[categorie]=[]
                listFondsByCat[categorie].append([nomFonds,categorie,perfo1an,perfo3ans,perfo5ans,perfo10ans,vol1an,vol3ans,vol5ans,sharp1an,sharp3ans,sharp5ans,perfo1mois,perfo3mois,perfo6mois,metrique])
            else:
                sss=listFondsByCat[categorie]
                sss.append([nomFonds,categorie,perfo1an,perfo3ans,perfo5ans,perfo10ans,vol1an,vol3ans,vol5ans,sharp1an,sharp3ans,sharp5ans,perfo1mois,perfo3mois,perfo6mois,metrique])
                listFondsByCat[categorie]=sss
        except:
            pass

    listeFondsTri=sorted(listeFonds,key=lambda x:-x[-1])
    if DEBUG==False:
        #print("Computation with 1 YEAR=",SCORE1AN,"3 YEARS=",SCORE3ANS,"5 YEAR=",SCORE5ANS)
        #print("================= 50000 best funds ====================")
        worksheet = workbook.add_worksheet("Best-Off")
        row = 0
        col = 0
        bold = workbook.add_format({'bold': True})
        worksheet.write(row, col, "Nom du fonds", bold)
        worksheet.write(row, col + 1, "Categorie", bold)
        worksheet.write(row, col + 2, "Perfo 1 an", bold)
        worksheet.write(row, col + 3, "Perfo 3 ans", bold)
        worksheet.write(row, col + 4, "Perfo 5 ans", bold)
        worksheet.write(row, col + 5, "Perfo 10 ans", bold)
        worksheet.write(row, col + 6, "Sharp 1 an", bold)
        worksheet.write(row, col + 7, "Sharp 3 ans", bold)
        worksheet.write(row, col + 8, "Sharp 5 ans", bold)
        worksheet.write(row, col + 9, "Perfo 1 mois", bold)
        worksheet.write(row, col + 10, "Perfo 3 mois", bold)
        worksheet.write(row, col + 11, "Perfo 6 mois", bold)
        worksheet.write(row, col + 12, "Perfo", bold)
        row += 1
        cpt=0
        nn=min(1000000,len(listeFondsTri)-1)
        for l in listeFondsTri[0:nn]:
            cpt=cpt+1
            print(strFund(l),cpt)
            worksheet.write(row, col, str(l[0]))  # nom
            worksheet.write(row, col + 1, str(l[1]))  # cat
            worksheet.write(row, col + 2, str(l[2]))  # 1an
            worksheet.write(row, col + 3, str(l[3]))  # 3ans"
            worksheet.write(row, col + 4, str(l[4]))  # 5 ans
            worksheet.write(row, col + 5, str(l[5]))  # 5 ans
            worksheet.write(row, col + 6, str(l[-7]))  # sharp
            worksheet.write(row, col + 7, str(l[-6]))  # sharp
            worksheet.write(row, col + 8, str(l[-5]))  # sharp
            worksheet.write(row, col + 9, str(l[-4]))  # perfo 1 mois
            worksheet.write(row, col + 10, str(l[-3]))  # perfo 3 mois
            worksheet.write(row, col + 11, str(l[-2]))  # perfo 6 mois
            worksheet.write(row, col + 12, str(l[-1]))  # metrique

            row+=1
        keylist=listFondsByCat.keys()
        keylist=sorted(keylist)
    if DEBUG==False:
        #print("\n\n\n\n=================Ranking by category=============")
        #for c in (listFondsByCat):
        for c in keylist:
            sheetname=c
            for ctoreplace in listecarreplace:
                sheetname=shortenname(sheetname).replace(ctoreplace,"")

            if len(sheetname)>31:
                sheetname=sheetname[0:28]

            if len(listFondsByCat[c])>-1:
                #print("============",c,"("+str(len(listFondsByCat[c]))+" funds)============")
                row = 0
                col = 0
                try:
                    worksheet = workbook.add_worksheet(sheetname)
                    #print("added",sheetname)
                except:
                    worksheet = workbook.add_worksheet(sheetname+(str(random.randint(1,100))))
                    #print("added with problem",sheetname)
                worksheet.write(row, col, "Nom du fonds",bold)
                worksheet.write(row, col + 1, "Categorie",bold)
                worksheet.write(row, col + 2, "Perfo 1 an", bold)
                worksheet.write(row, col + 3, "Perfo 3 ans", bold)
                worksheet.write(row, col + 4, "Perfo 5 ans", bold)
                worksheet.write(row, col + 5, "Perfo 10 ans", bold)
                worksheet.write(row, col + 6, "Sharp 1 an", bold)
                worksheet.write(row, col + 7, "Sharp 3 ans", bold)
                worksheet.write(row, col + 8, "Sharp 5 ans", bold)
                worksheet.write(row, col + 9, "Perfo 1 mois", bold)
                worksheet.write(row, col + 10, "Perfo 3 mois", bold)
                worksheet.write(row, col + 11, "Perfo 6 mois", bold)
                worksheet.write(row, col + 12, "Perfo",bold)
                row+=1
                perfoGlob=0
                cpt=0
                l=listFondsByCat[c]
                lTri=sorted(l,key=lambda x:-x[-1])
                for ll in lTri[0:NB]:
                    #print(strFund(ll))
                    worksheet.write(row, col, str(ll[0]))  # nom
                    worksheet.write(row, col + 1, str(ll[1]))  # cat
                    worksheet.write(row, col + 2, str(ll[2]))  # 1an
                    worksheet.write(row, col + 3, str(ll[3]))  # 3ans"
                    worksheet.write(row, col + 4, str(ll[4]))  # 5 ans
                    worksheet.write(row, col + 5, str(ll[5]))  # 5 ans
                    worksheet.write(row, col + 6, str(ll[-7]))  # sharp
                    worksheet.write(row, col + 7, str(ll[-6]))  # sharp
                    worksheet.write(row, col + 8, str(ll[-5]))  # sharp
                    worksheet.write(row, col + 9, str(ll[-4]))  # perfo 1 mois
                    worksheet.write(row, col + 10, str(ll[-3]))  # perfo 3 mois
                    worksheet.write(row, col + 11, str(ll[-2]))  # perfo 6 mois
                    worksheet.write(row, col + 12, str(ll[-1]))  # metrique
                    row+=1
                    #perfoGlob=perfoGlob+ll[-1]/len(lTri[0:NB])
                for ll in lTri[0:NBPERFO]:
                    perfoGlob=perfoGlob+ll[-1]/len(lTri[0:NBPERFO])
                    cpt+=1
                listPerfoCat.append([ll[1],perfoGlob,cpt])
        #print("\n\n\n\n=================Ranking of categories=============")
        worksheet = workbook.add_worksheet("Classement categories")
        lcatTri=sorted(listPerfoCat,key=lambda x:-x[1])
        row=0
        col=0
        for ll in lcatTri:
            v=round(Decimal(ll[1]),2)
            #print(ll[0],v,"computed on ",ll[2]," values")
            worksheet.write(row,col,str(ll[0]))
            worksheet.write(row, col+1, str(v))
            row+=1

    #print(cpterror,"lines could not be parsed")
    workbook.close()
