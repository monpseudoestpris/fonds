#!/usr/bin/env python3

import argparse
import subprocess


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="convert raw funds file to CSV")
    parser.add_argument("market", help="market to parse (e.g., EUR, USD)")
    parser.add_argument("poche", help="poche to parse (e.g., PEA, AV, ALL)")
    args = parser.parse_args()
    money=args.market
    poche=args.poche
    lance_process_opcvm360  = subprocess.call("python3 opcvm360Hidden.py "+money+ " "+poche, shell=True)  
    lance_process_parse  = subprocess.call("python3 parseMarket.py "+money, shell=True)  
    lance_process_analyse  = subprocess.call("python3 analyse.py "+money+" no", shell=True)  