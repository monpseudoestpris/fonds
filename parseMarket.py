#!/usr/bin/env python3

import argparse
import os
import re
import math
from decimal import Decimal
import sys
import xlsxwriter
import random

DOGETCATS=True
NB=3000
NBPERFO=10
SCORE1AN=0.333333333
SCORE3ANS=0.333333333
SCORE5ANS=0.333333333
INFINITE=999999
MAXVALUE=900
ZERO=0.0

DELTA=1

listreplace=["Actions","italisations","-","logie","ernatif","ectionnelle","mmation","munication","/","reprises","-","_","érales","reprises"]

listecarreplace=["'",'`',"!","?",",",";",":","''"]

liste_debuts_categorie=[x for x in open('listedebut.txt','r').readlines()]

liste_categories=[('OBLIGATION','obligations'),
(' BONDS','obligations'),
(' BOND','obligations'),
(' JAPAN','Actions Japon'),
(' ALLEMAGNE','Actions Allemagne'),
(' JAPON','Actions Japon'),
(' GERMANY','Actions Allemagne'),
(' CHINE','Actions Chine'),
(' TECH','Actions Technologie'),
(' US','Actions USA'),
(' EMERG','Actions Emergentes Monde'),
(' IMMO','Immobilier'),
(' PIERRE','Immobilier'),
(' ALGER','Actions Algerie'),
(' EUROP','Actions Europe'),
(' IRELAN','Actions Irelande'),
(' OR','Actions Monde Or & Métaux précieux'),
(' GOLD','Actions Monde Or & Métaux précieux'),
(' INNOVAT','Actions Technologie'),
(' DEFENS','Mixtes Europe Defensif'),
(' HEALTH','Actions Monde Santé')
]



def shortenname(sheetname):
    for r in listreplace:
        sheetname=sheetname.replace(r,"")
    sheetname=sheetname.lower()
    return(sheetname)

def getperfo(c,year):
    c=(float)(c)
    if c>MAXVALUE:
        c=0    
    try:
        valeur=1+c/100.0
    except:
        valeur=1.0
    
    out=(math.pow(valeur,1/year)-1)*100
    if out>MAXVALUE/2:
        out=0
    return(out)
def strFund(l):
    out=l[0]+" "+l[1]+" "+(str)(l[-1])
    return(out)

class Fonds:
    """Class to describe a fund"""

    def __init__(self,nom,vl,p1janv,p7j,p1mois,p3mois,p6mois,p1an,p3ans,p5ans,p10ans):
        self.nom=nom
        self.valorisation=vl
        self.perfo_1janv=p1janv
        self.perfo_7j=p7j
        self.perfo_1mois=p1mois
        self.perfo_3mois=p3mois
        self.perfo_6mois=p6mois
        self.perfo_1an=p1an
        self.perfo_3ans=p3ans
        self.perfo_5ans=p5ans
        self.perfo_10ans=p10ans
        self.categorie="No category"
    def display(self):
        try:
            l=[self.nom,self.categorie,self.valorisation,self.perfo_1janv,self.perfo_7j,self.perfo_1mois,self.perfo_3mois,self.perfo_6mois,self.perfo_1an,self.perfo_3ans,self.perfo_5ans,self.perfo_10ans, self.vol1an,self.vol3ans,self.vol5ans,self.sharp1an,self.sharp3ans,self.sharp5ans]
            ls = [(str)(x) for x in l]
            s=";".join(ls)
        except:
            s=""
        return(s)
    def add_categorie(self,c):
        c=c.replace("\n","")
        self.categorie=c
    def add_volasharp(self,v1,v2,v3,s1,s2,s3):
        self.vol1an=v1
        self.vol3ans=v2
        self.vol5ans=v3

        self.sharp1an=s1
        self.sharp3ans=s2
        self.sharp5ans=s3

def get_field(list,number,defaut):
    try:
        out=(float)(list[number])
    except:
        out=defaut
    return(out)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="convert raw funds file to CSV")
    parser.add_argument("market", help="market to parse (e.g., EUR, USD)")
    args = parser.parse_args()
    money=args.market
    fperfos = open("perfos"+money+".txt")
    fratios= open("ratios"+money+".txt")
    fperfglissantes= open("perfglissantes"+money+".txt")
    hash_Funds={}
    linesPerfGlissantes=[x for x in fperfglissantes.readlines()]
    linesPerfo=[x for x in fperfos.readlines()]
    linesRatio=[x for x in fratios.readlines()]
    header=linesPerfGlissantes[0]
    list_cat_connues=[]
    #on parcourt perf glissantes
    cpt1=0
    for fund in linesPerfGlissantes:
        cpt1+=1
        
        if fund.find('1 Mois')!=-1:
            continue
        fund = fund.strip()
        #on recupere la VL
        c_vl_short='[0-9]+\.[0-9]+ EUR'
        c_vl_long='[0-9]+\ [0-9]+\.[0-9]+ EUR'
        c_money=money
        if re.search(c_vl_long, fund):
            vl=re.search(c_vl_long, fund).group(0)
        elif re.search(c_vl_short, fund):
            vl=re.search(c_vl_short, fund).group(0)
        else:
            vl=re.search(c_money, fund).group(0)
        chaine_coupee=fund.split(vl)
        #on recupere le nom du fonds
        nom_fonds=chaine_coupee[0].strip()
        if nom_fonds=="":
            continue
        reste_chaine=chaine_coupee[1].split("%")

        evol_1janv=get_field(reste_chaine,0,-ZERO)
        evol_7j=get_field(reste_chaine,1,-ZERO)
        evol_1mois=getperfo(get_field(reste_chaine,2,-ZERO),1/12)
        evol_3mois=getperfo(get_field(reste_chaine,3,-ZERO),1/4)
        evol_6mois=getperfo(get_field(reste_chaine,4,-ZERO),0.5)
        evol_1an=get_field(reste_chaine,5,-ZERO)
        evol_3ans=getperfo(get_field(reste_chaine,6,-ZERO),3)
        evol_5ans=getperfo(get_field(reste_chaine,7,-ZERO),5)
        evol_10ans=getperfo(get_field(reste_chaine,8,-ZERO),10)

        f=Fonds(nom_fonds,vl,evol_1janv,evol_7j,evol_1mois,evol_3mois,evol_6mois,evol_1an,evol_3ans,evol_5ans,evol_10ans)
        # if nom_fonds in hash_cat_connues:
        #     ##print("trying",nom_fonds)ls 
        #     f.add_categorie(hash_cat_connues[nom_fonds])
        #     list_cat_connues.append(nom_fonds)
        hash_Funds[nom_fonds]=f
    list_cat_pas_connues=[]
    for ll in hash_Funds:
        ##print("on",ll,list_cat_connues)
        if not(ll.strip() in list_cat_connues):
            list_cat_pas_connues.append(ll.strip())
    cpt=0
    liste_cat_deb=[]
    cpt1=0
    if DOGETCATS:
        for fund in linesPerfo:
            print("=======================================================",fund)
            trouve=False
            cpt+=1
            cpt1+=1
            
                
            if fund.find('Nom Cat')!=-1:
                continue
            c_vl_short='[0-9]+\.[0-9]+ EUR'
            c_vl_long='[0-9]+\ [0-9]+\.[0-9]+ EUR'
            c_money=money
            if re.search(c_vl_long, fund):
                vl=re.search(c_vl_long, fund).group(0)
            elif re.search(c_vl_short, fund):
                vl=re.search(c_vl_short, fund).group(0)
            else:
                vl=re.search(c_money, fund).group(0)
            chaine_coupee=fund.split(vl)
            #on recupere le nom du fonds
            nom_fonds=chaine_coupee[0].strip()
            reste_chaine=chaine_coupee[1].split("%")
            n=0
            
            while(trouve==False and n<20):
                for k in list_cat_pas_connues:
                    #print("---------------------------------------------",k)
                    l1=max(len(k.split(' '))-4,2)
                    l2=max(len(nom_fonds.split(' '))-4,2)
                    a1="".join(k.split(' ')[:min(l1,l2)])
                    a2="".join(nom_fonds.split(' ')[:min(l1,l2)])
                    lenk=len(k)
                    nok=n
                    if n> lenk*0.33:
                        nok=(int)(lenk*0.33)
                    #print("A1A2",a1,a2,nok)
                    if a1==a2:
                        if nok==0:
                            k_short=k
                        else:
                            k_short=k[:-nok]

                        #print("trying",k_short,nok,"VS",fund)
                        where=nom_fonds.find(k_short)
                        #print("where",where)
                        if where==0:
                            categorie=nom_fonds.replace(k_short,"").strip()
                            ccatshort='\([A-Z]+\)'
                            if re.search(ccatshort, categorie):
                                #print('CATEGORY PROBLEM !!!!!!!!!')
                                categorie='No category'
                            ccatshort='\([A-Z]+.\)'
                            if re.search(ccatshort, categorie):
                                #print('CATEGORY PROBLEM !!!!!!!!!')
                                categorie='No category'
                            #print("CATEGORIE",categorie)
                            f=hash_Funds[k]
                            f.add_categorie(categorie)
                            hash_Funds[k]=f
                            trouve=True
                            #print("TROUVE:",k,"/",categorie)
                            ccat='[A-Z][a-z]+'
                            catd="No category"
                            if re.search(ccat, categorie):
                                catd=re.search(ccat, categorie).group(0)
                                #print("CATD",catd)
                            
                            categorie_split=categorie.split(' ')
                            #print("CATEGORIE SPLIT",categorie_split)
                            try:
                                where_catd=categorie_split.index(catd)
                            except:
                                where_catd=0
                            #print("WHERECATD",where_catd)
                            list_ok=categorie_split[where_catd:]
                            #print("LISTEOK",list_ok)
                            categorie_ok=" ".join(list_ok)
                            #categorie_ok=categorie_ok.replace(" ","")
                            #print("CATEGORY;",categorie_ok,";",fund)
                            ii=categorie_ok in [""," ","  "]
                            if ii:
                                categorie_ok="No category"
                                for possible in liste_categories:
                                    to_find=possible[0]
                                    to_replace=possible[1]
                                    if k.find(to_find)>=0:
                                        categorie_ok=to_replace
                                    else:
                                        if k.find("-"+to_find)>=0:
                                            categorie_ok=to_replace
                                        else:
                                            if k.find(to_find[1:])==0:
                                                categorie_ok=to_replace

                            f=hash_Funds[k]
                            f.add_categorie(categorie_ok)
                            break
                n=n+1
    for l in linesRatio:
        if l.find('ans')!=-1 or l.find('Sharp')!=-1:
            continue
        c_vl_short='[0-9]+\.[0-9]+ EUR'
        c_vl_long='[0-9]+\ [0-9]+\.[0-9]+ EUR'
        c_money=money
        if re.search(c_vl_long, l):
            vl=re.search(c_vl_long, l).group(0)
        elif re.search(c_vl_short, l):
            vl=re.search(c_vl_short, l).group(0)
        else:
            vl=re.search(c_money, l).group(0)
        chaine_coupee=l.split(vl)
        nom_fonds=chaine_coupee[0].strip()
        
        #ls=l.split(money)[-1].split('%')
        ls=l.split(money)[-1].lstrip().replace("%","").replace("  "," ").split(" ")
        print("ls",ls)
        try:
            vol1an=(float)(ls[0])
        except:
            vol1an=9999
        try:
            vol3ans=(float)(ls[1])
        except:
            vol3ans=9999
        try:
            vol5ans=(float)(ls[2])
        except:
            vol5ans=9999
        try:
            sharp1an=(float)(ls[3])
        except:
            sharp1an=-9999
        try:
            sharp3ans=(float)(ls[4])
        except:
            sharp3ans=-9999
        try:
            sharp5ans=(float)(ls[5])
        except:
            sharp5ans=-9999
        print("sharps",sharp1an,sharp3ans,sharp5ans)
        #sharp ratio
        # try:
        #     sharp1an=(float)(ls[3][1:].split(' ')[0])
        # except:
        #     sharp1an=-9999
        # try:
        #     sharp3ans=(float)(ls[3][1:].split(' ')[1])
        # except:
        #     sharp3ans=-9999
        # try:
        #     sharp5ans=(float)(ls[3][1:].split(' ') [2])
        # except:
        #     sharp5ans=-9999
        # print("sharps",sharp1an,sharp3ans,sharp5ans)
        # if sharp1an==-9999 and sharp3ans==-9999 and sharp5ans==-9999:
        #     print("retry sharp 1",ls)
        #     try:
        #         sharp1an=(float)(ls[2][1:].split(' ')[0])
        #     except:
        #         sharp1an=-9999
        #     try:
        #         sharp3ans=(float)(ls[2][1:].split(' ')[1])
        #     except:
        #         sharp3ans=-9999
        #     try:
        #         sharp5ans=(float)(ls[2][1:].split(' ') [2])
        #     except:
        #         sharp5ans=-9999
        #     print("sharps",sharp1an,sharp3ans,sharp5ans)
        #     if sharp1an==-9999 and sharp3ans==-9999 and sharp5ans==-9999:
        #         print("retry sharp 2",ls)
        #         try:
        #             sharp1an=(float)(ls[1][1:].split(' ')[0])
        #         except:
        #             sharp1an=-9999
        #         try:
        #             sharp3ans=(float)(ls[1][1:].split(' ')[1])
        #         except:
        #             sharp3ans=-9999
        #         try:
        #             sharp5ans=(float)(ls[1][1:].split(' ') [2])
        #         except:
        #             sharp5ans=-9999
        #         print("sharps",sharp1an,sharp3ans,sharp5ans)

        try:
            f=hash_Funds[nom_fonds]
            f.add_volasharp(vol1an,vol3ans,vol5ans,sharp1an,sharp3ans,sharp5ans)
            hash_Funds[nom_fonds]=f
        except:
            pass
            
        ##print("---->PERF",nom_fonds,vl,reste_chaine)   
#ecriture de la hash table
    with open('sortie'+money+'.txt', 'w') as outfile:
        for k in hash_Funds:
            d=hash_Funds[k].display()
            outfile.write(d+'\n')
            
         

    fperfos.close()
    fratios.close()
    fperfglissantes.close()


